/****************************************************************************
** Meta object code from reading C++ file 'qtscriptdebug.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.2.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "qtscriptdebug.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qtscriptdebug.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.2.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ScriptDebugger_t {
    QByteArrayData data[22];
    char stringdata[335];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_ScriptDebugger_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_ScriptDebugger_t qt_meta_stringdata_ScriptDebugger = {
    {
QT_MOC_LITERAL(0, 0, 14),
QT_MOC_LITERAL(1, 15, 15),
QT_MOC_LITERAL(2, 31, 0),
QT_MOC_LITERAL(3, 32, 3),
QT_MOC_LITERAL(4, 36, 12),
QT_MOC_LITERAL(5, 49, 10),
QT_MOC_LITERAL(6, 60, 3),
QT_MOC_LITERAL(7, 64, 12),
QT_MOC_LITERAL(8, 77, 12),
QT_MOC_LITERAL(9, 90, 5),
QT_MOC_LITERAL(10, 96, 20),
QT_MOC_LITERAL(11, 117, 19),
QT_MOC_LITERAL(12, 137, 18),
QT_MOC_LITERAL(13, 156, 19),
QT_MOC_LITERAL(14, 176, 17),
QT_MOC_LITERAL(15, 194, 21),
QT_MOC_LITERAL(16, 216, 20),
QT_MOC_LITERAL(17, 237, 18),
QT_MOC_LITERAL(18, 256, 20),
QT_MOC_LITERAL(19, 277, 19),
QT_MOC_LITERAL(20, 297, 19),
QT_MOC_LITERAL(21, 317, 16)
    },
    "ScriptDebugger\0labelClickedIdx\0\0idx\0"
    "labelClicked\0runClicked\0obj\0updateModels\0"
    "powerEditing\0value\0powerEditingFinished\0"
    "playerButtonClicked\0droidButtonClicked\0"
    "structButtonClicked\0featButtonClicked\0"
    "researchButtonClicked\0sensorsButtonClicked\0"
    "deityButtonClicked\0weatherButtonClicked\0"
    "revealButtonClicked\0shadowButtonClicked\0"
    "fogButtonClicked\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ScriptDebugger[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   99,    2, 0x09,
       4,    0,  102,    2, 0x09,
       5,    1,  103,    2, 0x09,
       7,    0,  106,    2, 0x09,
       8,    1,  107,    2, 0x09,
      10,    0,  110,    2, 0x09,
      11,    1,  111,    2, 0x09,
      12,    0,  114,    2, 0x09,
      13,    0,  115,    2, 0x09,
      14,    0,  116,    2, 0x09,
      15,    0,  117,    2, 0x09,
      16,    0,  118,    2, 0x09,
      17,    0,  119,    2, 0x09,
      18,    0,  120,    2, 0x09,
      19,    0,  121,    2, 0x09,
      20,    0,  122,    2, 0x09,
      21,    0,  123,    2, 0x09,

 // slots: parameters
    QMetaType::Void, QMetaType::QModelIndex,    3,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QObjectStar,    6,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void ScriptDebugger::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ScriptDebugger *_t = static_cast<ScriptDebugger *>(_o);
        switch (_id) {
        case 0: _t->labelClickedIdx((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 1: _t->labelClicked(); break;
        case 2: _t->runClicked((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 3: _t->updateModels(); break;
        case 4: _t->powerEditing((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->powerEditingFinished(); break;
        case 6: _t->playerButtonClicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->droidButtonClicked(); break;
        case 8: _t->structButtonClicked(); break;
        case 9: _t->featButtonClicked(); break;
        case 10: _t->researchButtonClicked(); break;
        case 11: _t->sensorsButtonClicked(); break;
        case 12: _t->deityButtonClicked(); break;
        case 13: _t->weatherButtonClicked(); break;
        case 14: _t->revealButtonClicked(); break;
        case 15: _t->shadowButtonClicked(); break;
        case 16: _t->fogButtonClicked(); break;
        default: ;
        }
    }
}

const QMetaObject ScriptDebugger::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ScriptDebugger.data,
      qt_meta_data_ScriptDebugger,  qt_static_metacall, 0, 0}
};


const QMetaObject *ScriptDebugger::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ScriptDebugger::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ScriptDebugger.stringdata))
        return static_cast<void*>(const_cast< ScriptDebugger*>(this));
    return QDialog::qt_metacast(_clname);
}

int ScriptDebugger::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 17;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
